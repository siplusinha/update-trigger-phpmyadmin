

--
-- Table structure for table `batch`
--

CREATE TABLE `batch` (
  `Batch_id` int(10) NOT NULL,
  `Batch_name` varchar(50) NOT NULL,
  `Room_id` int(10) DEFAULT NULL,
  `Shift` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`Batch_id`, `Batch_name`, `Room_id`, `Shift`) VALUES
(1, 'CIS1', NULL, 'Morning'),
(2, 'CIS2', NULL, 'Evening');


-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `Room_id` int(10) NOT NULL,
  `Num_of_seat` int(10) DEFAULT NULL,
  `Room_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`Room_id`, `Num_of_seat`, `Room_status`) VALUES
(101, 35, 'unassigned'),
(102, 40, 'unassigned');